<?php

include_once('../../../vendor/autoload.php');
use App\profile_picture\Profile_Picture;
use App\Message\Message;
use App\Utility\Utility;
$objProfile_Picture= new Profile_Picture();

$id=$_POST['id'];

if($_SERVER['REQUEST_METHOD']=='POST')
{
    if(preg_match("/([A-Za-z0-9-_])/",$_POST['user_name'])) {
        $objProfile_Picture->setData($_POST);
        if (empty($_FILES['FileToUpload']['name']))
        {
            $image_name=basename($_POST['pic']);
            $temporary_location="C:\xampp\tmp\php5EF3.tmp";
            move_uploaded_file($temporary_location,'../../../resource/Picture/'.$image_name);
            $objProfile_Picture->user_profile_picture='resource/Picture/'.$image_name;
        }
        else
        {
            $objProfile_Picture->setData($_FILES);
        }
        $objProfile_Picture->update();
    }
    else{
        Message::message("<div id='message'><h3 align='center'>Invalid Input !</h3></div>");
        header("location:edit.php?id=$id");
    }

}
else{
    Message::message("<div id='message'><h3 align='center'> Oops something went wrong !</h3></div>");
    Utility::redirect('index.php');
}