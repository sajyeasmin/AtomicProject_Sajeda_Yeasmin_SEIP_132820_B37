<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\profile_picture\Profile_Picture;

if(!isset( $_SESSION)) session_start();
echo "<div id=\"message\">". Message::message()."</div>";

$objProfile_Picture=new Profile_Picture();
$objProfile_Picture->setData($_GET);
$singleItem= $objProfile_Picture->view("obj");

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Update- Profile Picture - Formoid css form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Bootstrap Core CSS -->
    <link href="../../../resource/left_nevigation_asset/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../resource/left_nevigation_asset/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../../../resource/left_nevigation_asset/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../resource/left_nevigation_asset/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body class="blurBg-false" background="../../../resource/background.jpg">
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Navigation -->
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Atomic Project</a>
            </div>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="index.php"><i class="fa fa-fw fa-book"></i>Book Title</a>
                    </li>
                    <li>
                        <a href="../Birthday/index.php"><i class="fa fa-fw fa-birthday-cake"></i>Birthday</a>
                    </li>
                    <li>
                        <a href="../City/index.php"><i class="fa fa-fw fa-building"></i>City</a>
                    </li>
                    <li>
                        <a href="../Email/index.php"><i class="fa fa-fw fa-envelope"></i>Email</a>
                    </li>
                    <li>
                        <a href="../Gender/index.php"><i class="fa fa-fw fa-female" aria-hidden="true"></i>Gender</a>
                    </li>
                    <li>
                        <a href="../Hobbies/index.php"><i class="fa fa-fw fa-gamepad"></i>Hobbies</a>
                    </li>
                    <li>
                        <a href="../Profile_Picture/index.php"><i class="fa fa-fw fa-user"></i>Profile Picture</a>
                    </li>
                    <li>
                        <a href="../Summary_Of_Organization/index.php"><i class="fa fa-fw fa-group"></i>Summary of Organization</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
</div>
<!-- Start Formoid form-->
<link rel="stylesheet" href="../../../resource/form_booktitle_files/formoid1/formoid-solid-dark.css" type="text/css" />
<script type="text/javascript" src="../../../resource/form_booktitle_files/formoid1/jquery.min.js"></script>

<div class="container" style="width:1000px; margin-left: 250px;">

    <div style="width: 1000px; height: 60px;margin-left: 100px;margin-top: 50px">
        <a href="index.php" class="btn btn-primary" role="button">Back To List</a>
    </div>

<form enctype="multipart/form-data" class="formoid-solid-dark" action="update.php" method="post" style="background-color:#ffffff;font-size:14px;font-family:'Roboto',Arial,Helvetica,sans-serif;
		color:#34495E;max-width:780px;min-width:150px"><div class="title"><h2>Update- Profile Picture</h2></div>
    <input type="hidden" name="id" value="<?php echo $singleItem->id ?>">
    <input type="hidden" name="pic" value="<?php echo $singleItem->user_profile_picture ?>">
    <div class="element-input" title="Please Enter a Your Name."><label class="title"><span class="required">*</span></label><div class="item-cont"><input class="large" type="text" name="user_name" required="required" value="<?php echo $singleItem->user_name ?>"/><span class="icon-place"></span></div></div>
    <div><img src="<?php echo "../../../".$singleItem->user_profile_picture ?>" width="50" height="50" alt="flower"></div>
    <div class="element-file"><label class="title"></label><div class="item-cont"><label class="large"><div class="button">Choose File</div>
    <input type="file" class="file_input" name="FileToUpload" /><div class="file_text">Choose file to upload</div><span class="icon-place"></span></label></div></div>
    <br>

    <div class="submit">

        <input type="submit"  value="Update"/>
    </div>
    <br>
</form>
</div>
<p class="frmd"><a href="http://formoid.com/v29.php">css form</a> Formoid.com 2.9</p><script type="text/javascript" src="../../../resource/form_booktitle_files/formoid1/formoid-solid-dark.js"></script>
<!-- Stop Formoid form-->

</body>
</html>
