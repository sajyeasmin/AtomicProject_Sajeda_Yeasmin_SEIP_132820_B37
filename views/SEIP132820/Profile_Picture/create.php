<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Add- Profile Picture - Formoid css form</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
			$("#msg").delay(2500).fadeOut("slow");
		});
	</script>
</head>

<body class="blurBg-false" style="background-color:#EBEBEB">

<!-- Start Formoid form-->
<link rel="stylesheet" href="../../../resource/form_booktitle_files/formoid1/formoid-solid-dark.css" type="text/css" />
<script type="text/javascript" src="../../../resource/form_booktitle_files/formoid1/jquery.min.js"></script>

<form enctype="multipart/form-data" class="formoid-solid-dark" action="store.php" method="post" style="background-color:#ffffff;font-size:14px;font-family:'Roboto',Arial,Helvetica,sans-serif;
		color:#34495E;max-width:780px;min-width:150px"><div class="title"><h2>Add- Profile Picture</h2></div>

	<div class="element-input" title="Please Enter a Your Name."><label class="title"><span class="required">*</span></label><div class="item-cont"><input class="large" type="text" name="user_name" required="required" placeholder="User Name"/><span class="icon-place"></span></div></div>
	<div class="element-file"><label class="title"></label><div class="item-cont"><label class="large"><div class="button">Choose File</div><input type="file" class="file_input" name="FileToUpload" /><div class="file_text">No file selected</div><span class="icon-place"></span></label></div></div>
	<br>

	<div class="submit">

		<input type="submit"  value="Save"/>
		<input type="submit" value="Save and Add New"/>
		<input type="reset"  value="Reset"/>
		<input type="submit" value="Back To List"/>
	</div>
	<br>
</form>
<p class="frmd"><a href="http://formoid.com/v29.php">css form</a> Formoid.com 2.9</p><script type="text/javascript" src="../../../resource/form_booktitle_files/formoid1/formoid-solid-dark.js"></script>
<!-- Stop Formoid form-->

</body>
</html>
