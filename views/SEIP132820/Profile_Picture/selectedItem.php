<?php
require_once("../../../vendor/autoload.php");
use App\profile_picture\Profile_Picture;

$objPicture=new Profile_Picture();


if(isset($_POST['delete'])) {

    for ($i = 0; $i < count($_POST['checkbox']); $i++) {
        $del_id = $_POST['checkbox'][$i];
        $objPicture->deleteSelected($del_id);
    }
}
else if(isset($_POST['trash'])){
    for ($i = 0; $i < count($_POST['checkbox']); $i++) {
        $trash_id = $_POST['checkbox'][$i];
        $objPicture->trashSelected($trash_id);
    }
}
else if(isset($_POST['recovery'])){
    for ($i = 0; $i < count($_POST['checkbox']); $i++) {
        $trash_id = $_POST['checkbox'][$i];
        $objPicture->restoreSelected($trash_id);
    }
}