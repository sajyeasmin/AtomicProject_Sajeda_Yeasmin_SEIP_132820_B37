<?php
require_once("../../../vendor/autoload.php");
use App\book_title\BookTitle;

$objBookTitle=new BookTitle();


if(isset($_POST['delete'])) {

    for ($i = 0; $i < count($_POST['checkbox']); $i++) {
        $del_id = $_POST['checkbox'][$i];
        $objBookTitle->deleteSelected($del_id);
    }
}
else if(isset($_POST['trash'])){
    for ($i = 0; $i < count($_POST['checkbox']); $i++) {
        $trash_id = $_POST['checkbox'][$i];
        $objBookTitle->trashSelected($trash_id);
    }
}
else if(isset($_POST['recovery'])){
    for ($i = 0; $i < count($_POST['checkbox']); $i++) {
        $trash_id = $_POST['checkbox'][$i];
        $objBookTitle->restoreSelected($trash_id);
    }
}