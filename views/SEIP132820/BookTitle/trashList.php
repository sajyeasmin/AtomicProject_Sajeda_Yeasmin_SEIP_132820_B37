<?php
require_once("../../../vendor/autoload.php");
use App\book_title\BookTitle;
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo "<div id=\"message\">". Message::message()."</div>";

$objBookTitle=new BookTitle();
$objBookTitle->setData($_GET);
$allData= $objBookTitle->trashList("obj");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Bootstrap Core CSS -->
    <link href="../../../resource/left_nevigation_asset/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../resource/left_nevigation_asset/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../../../resource/left_nevigation_asset/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../resource/left_nevigation_asset/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>
<body background="../../../resource/background.jpg">

<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Navigation -->
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Atomic Project</a>
            </div>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="index.php"><i class="fa fa-fw fa-book"></i>Book Title</a>
                    </li>
                    <li>
                        <a href="../Birthday/index.php"><i class="fa fa-fw fa-birthday-cake"></i>Birthday</a>
                    </li>
                    <li>
                        <a href="../City/index.php"><i class="fa fa-fw fa-building"></i>City</a>
                    </li>
                    <li>
                        <a href="../Email/index.php"><i class="fa fa-fw fa-envelope"></i>Email</a>
                    </li>
                    <li>
                        <a href="../Gender/index.php"><i class="fa fa-fw fa-female" aria-hidden="true"></i>Gender</a>
                    </li>
                    <li>
                        <a href="../Hobbies/index.php"><i class="fa fa-fw fa-gamepad"></i>Hobbies</a>
                    </li>
                    <li>
                        <a href="../Profile_Picture/index.php"><i class="fa fa-fw fa-user"></i>Profile Picture</a>
                    </li>
                    <li>
                        <a href="../Summary_Of_Organization/index.php"><i class="fa fa-fw fa-group"></i>Summary of Organization</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
</div>

<div class="container" style="width:1000px; margin-left: 250px;">
    <h2>Trash Item List</h2>

    <form name="form" id="form" method="post" action="selectedItem.php" onSubmit="return validate();">

    <div style="width: 1000px; height: 60px;margin-left: 0px;margin-top: 50px">
        <input class="btn btn-primary" name="recovery" type="submit" id="recovery" value="Recover Selected">
    </div>
    <div class="table-responsive">
        <table class="table table-bordered">

            <thead>
            <tr>
                <th>Check Box</th>
                <th>Serial No</th>
                <th>Id</th>
                <th>Book Title</th>
                <th>Author Name</th>
                <th>Operation</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $serial=0;
            foreach($allData as $oneData)
            {
                ?>
                <tr >
                    <td><input name="checkbox[]" type="checkbox" id="checkbox[]" value="<?php echo $oneData->id; ?>"></td>
                    <td ><?php echo ++$serial; ?></td >
                    <td ><?php echo $oneData->id; ?></td >
                    <td ><?php echo $oneData->book_title; ?></td >
                    <td ><?php echo $oneData->author_name; ?></td >
                    <td><a href="restore.php?id=<?php echo $oneData->id; ?>"><button type="button" class="btn btn-success btn-md" name="restore">Restore</button></a>
                        <a href="delete.php?id=<?php echo $oneData->id; ?>" onclick="return checkDelete()"><button type="button" class="btn btn-danger btn-md" name="delete">Permanent Delete</button></a></td>
                </tr >
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    </form>
</div>

</body>
</html>
<script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Are You Sure Delete?');
    }
</script>
<script language="javascript">
    function validate()
    {
        var chks = document.getElementsByName('checkbox[]');
        var hasChecked = false;
        for (var i = 0; i < chks.length; i++)
        {
            if (chks[i].checked)
            {
                hasChecked = true;
                break;
            }
        }
        if (hasChecked == false)
        {
            alert("Please select at least one.");
            return false;
        }
        return true;
    }
</script>