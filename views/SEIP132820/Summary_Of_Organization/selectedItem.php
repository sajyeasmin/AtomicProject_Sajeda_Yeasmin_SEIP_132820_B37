<?php
require_once("../../../vendor/autoload.php");
use App\summary_of_organization\Summary_Of_Organization;

$objOrganization=new Summary_Of_Organization();


if(isset($_POST['delete'])) {

    for ($i = 0; $i < count($_POST['checkbox']); $i++) {
        $del_id = $_POST['checkbox'][$i];
        $objOrganization->deleteSelected($del_id);
    }
}
else if(isset($_POST['trash'])){
    for ($i = 0; $i < count($_POST['checkbox']); $i++) {
        $trash_id = $_POST['checkbox'][$i];
        $objOrganization->trashSelected($trash_id);
    }
}
else if(isset($_POST['recovery'])){
    for ($i = 0; $i < count($_POST['checkbox']); $i++) {
        $trash_id = $_POST['checkbox'][$i];
        $objOrganization->restoreSelected($trash_id);
    }
}