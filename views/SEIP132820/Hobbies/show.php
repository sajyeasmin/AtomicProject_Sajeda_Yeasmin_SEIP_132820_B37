<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\hobbies\Hobbies;

$objHobbies= new Hobbies();
$objHobbies->setData($_GET);
$oneData=$objHobbies->show("obj");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Single Item View</h2>
    <div class="table-responsive">
    <table class="table table-bordered">

        <thead>
        <tr>
            <th>Id</th>
            <th>User Name</th>
            <th>Hobbies</th>
        </tr>
        </thead>
        <tbody>
            <tr >
                <td ><?php echo $oneData->id; ?></td >
                <td ><?php echo $oneData->user_name; ?></td >
                <td ><?php echo $oneData->user_hobbies; ?></td >
            </tr >
</tbody>
</table>
</div>
</div>

</body>
</html>