<?php
require_once("../../../vendor/autoload.php");
use App\hobbies\Hobbies;
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo "<div id=\"message\">". Message::message()."</div>";

$objHobbies=new Hobbies();
$objHobbies->setData($_GET);
$allData= $objHobbies->trashList("obj");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container" style="width:800px; margin:0 auto;">
    <h2>Trash Item List</h2>
    <div class="table-responsive">
        <table class="table table-bordered">

            <thead>
            <tr>
                <th>Serial No</th>
                <th>Id</th>
                <th>User Name</th>
                <th>Hobbies</th>
                <th>Operation</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $serial=0;
            foreach($allData as $oneData)
            {
                ?>
                <tr >
                    <td ><?php echo ++$serial; ?></td >
                    <td ><?php echo $oneData->id; ?></td >
                    <td ><?php echo $oneData->user_name; ?></td >
                    <td ><?php echo $oneData->user_hobbies; ?></td >
                    <td><a href="restore.php?id=<?php echo $oneData->id; ?>"><button type="button" class="btn btn-success btn-md" name="restore">Restore</button></a>
                        <a href="delete.php?id=<?php echo $oneData->id; ?>" onclick="return checkDelete()"><button type="button" class="btn btn-danger btn-md" name="delete">Permanent Delete</button></a></td>
                </tr >
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>
<script language="JavaScript" type="text/javascript">
    function checkDelete() {
        return confirm('Are You Sure Delete?');
    }
</script>