<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\hobbies\Hobbies;

if(!isset( $_SESSION)) session_start();
echo "<div id=\"message\">". Message::message()."</div>";

$objHobbies=new Hobbies();
$objHobbies->setData($_GET);
$singleItem= $objHobbies->view("obj");
$hobbies=explode(",",$singleItem->user_hobbies);

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Update- Hobbies - Formoid css form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>

<body class="blurBg-false" style="background-color:#EBEBEB">

<!-- Start Formoid form-->
<link rel="stylesheet" href="../../../resource/form_booktitle_files/formoid1/formoid-solid-dark.css" type="text/css" />
<script type="text/javascript" src="../../../resource/form_booktitle_files/formoid1/jquery.min.js"></script>

<form id="formoid" class="formoid-solid-dark" action="update.php" method="post" style="background-color:#ffffff;font-size:14px;font-family:'Roboto',Arial,Helvetica,sans-serif;
		color:#34495E;max-width:780px;min-width:150px"><div class="title"><h2>Update- Hobbies</h2></div>
    <input type="hidden" name="id" value="<?php echo $singleItem->id ?>">
    <div class="element-input" title="Please Enter Your Name."><label class="title"><span class="required">*</span></label><div class="item-cont"><input class="large" type="text" name="user_name" required="required" value="<?php echo $singleItem->user_name ?>"/><span class="icon-place"></span></div></div>
    <div class="element-checkbox"><label class="title">Hobbies</label>		<div class="column column1">

    <?php
    $dance=0;$singing=0;$drawing=0;$programming=0;
    foreach ($hobbies as $hobby)
    { ?>
        <?php if(!strcmp($hobby,"Programming")){ ++$programming; ?>
        <label><input type="checkbox" name="user_hobbies[]" value="Programming" checked /><span>Programming</span></label>
        <?php }?>

        <?php if(!strcmp($hobby,"Dance")){ ++$dance; ?>
        <label><input type="checkbox" name="user_hobbies[]" value="Dance" checked /><span>Dance</span></label>
        <?php }?>

        <?php if(!strcmp($hobby,"Singing")){ ++$singing;  ?>
        <label><input type="checkbox" name="user_hobbies[]" value="Singing" checked/><span>Singing</span></label>
        <?php }?>

        <?php if(!strcmp($hobby,"Drawing")){ ++$drawing; ?>
        <label><input type="checkbox" name="user_hobbies[]" value="Drawing" checked/><span>Drawing</span></label>
        <?php  }?>

    <?php
    } ?>

    <?php if($programming==0){ ?>
        <label><input type="checkbox" name="user_hobbies[]" value="Programming" /><span>Programming</span></label><?php } ?>
    <?php if($dance==0){ ?>
        <label><input type="checkbox" name="user_hobbies[]" value="Dance" /><span>Dance</span></label><?php } ?>
    <?php if($singing==0){ ?>
        <label><input type="checkbox" name="user_hobbies[]" value="Singing"/><span>Singing</span></label><?php } ?>
    <?php if($drawing==0){ ?>
        <label><input type="checkbox" name="user_hobbies[]" value="Drawing"/><span>Drawing</span></label><?php } ?>

        </div><span class="clearfix"></span>
    </div>

    <br>
    <div class="submit">
        <input type="submit" name="action" value="Update"/>
    </div>
    <br>
</form>
<p class="frmd"><a href="http://formoid.com/v29.php">css form</a> Formoid.com 2.9</p><script type="text/javascript" src="../../../resource/form_booktitle_files/formoid1/formoid-solid-dark.js"></script>
<!-- Stop Formoid form-->

</body>
</html>
