<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\city\City;

if(!isset( $_SESSION)) session_start();
echo "<div class=\"alert alert-success\" id=\"message\">". Message::message()."</div>";

$objCity= new City();
$allData = $objCity->index("obj");

$serial=1;

################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$allData =  $objCity->search($_REQUEST);
$availableKeywords=$objCity->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################

######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);

if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objCity->indexPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;
####################### pagination code block#1 of 2 end #########################################

################## search  block 2 of 5 start ##################
if(isset($_REQUEST['search']) ) {
    $someData = $objCity->search($_REQUEST);
    $serial = 1;
}
################## search  block 2 of 5 end ##################

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="../../../resource/left_nevigation_asset/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../resource/left_nevigation_asset/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../../../resource/left_nevigation_asset/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../resource/left_nevigation_asset/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../resource/bootstrap/css/jquery-ui.css">
    <script src="../../../resource/bootstrap/js/jquery-1.12.4.js"></script>
    <script src="../../../resource/bootstrap/js/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->
</head>
<body background="../../../resource/background.jpg">

<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Navigation -->
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">SB Admin</a>
            </div>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="../BookTitle/index.php"><i class="fa fa-fw fa-book"></i>Book Title</a>
                    </li>
                    <li>
                        <a href="../Birthday/index.php"><i class="fa fa-fw fa-birthday-cake"></i>Birthday</a>
                    </li>
                    <li class="active">
                        <a href="index.php"><i class="fa fa-fw fa-building"></i>City</a>
                    </li>
                    <li>
                        <a href="../Email/index.php"><i class="fa fa-fw fa-envelope"></i>Email</a>
                    </li>
                    <li>
                        <a href="../Gender/index.php"><i class="fa fa-fw fa-female"></i>Gender</a>
                    </li>
                    <li>
                        <a href="../Hobbies/index.php"><i class="fa fa-fw fa-gamepad"></i>Hobbies</a>
                    </li>
                    <li>
                        <a href="../Profile_Picture/index.php"><i class="fa fa-fw fa-user"></i>Profile Picture</a>
                    </li>
                    <li>
                        <a href="../Summary_Of_Organization/index.php"><i class="fa fa-fw fa-group"></i>Summary of Organization</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
</div>

<div class="container" style="width:1000px; margin-left: 300px;">
    <h2>City</h2>
    <!-- required for search, block 4 of 5 start -->
    <form id="searchForm" action="index.php"  method="get">
        <input type="text" value="" id="searchID" name="search" placeholder="Search" width="60" >
        <input type="checkbox"  name="byTitle"   checked  >By User Name
        <input type="checkbox"  name="byAuthor"  checked >By City
        <input hidden type="submit" class="btn-primary" value="search">
    </form>
    <!-- required for search, block 4 of 5 end -->

    <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
    <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
    <a href="email.php?list=1" class="btn btn-primary" role="button">Email to friend</a>

    <form name="form" id="form" method="post" action="selectedItem.php" onSubmit="return validate();">

        <div style="width: 800px; height: 60px;margin-left: 450px">
            <a href="create.php"><button type="button" class="btn btn-success btn-md" name="create">Add New Book</button></a>
            <input class="btn btn-danger btn-md" name="delete" type="submit" id="delete" value="Delete Selected">
            <input class="btn btn-warning btn-md" name="trash" type="submit" id="trash" value="Trash Selected" />
            <a href="trashList.php"><button type="button" class="btn btn-primary btn-md" name="trashList">Show Trash List</button></a>

        </div>
    <div class="table-responsive">
    <table class="table table-bordered">

        <thead>
        <tr>
            <th>Check Box</th>
            <th>Serial No</th>
            <th>Id</th>
            <th>User Name</th>
            <th>City</th>
            <th>Operation</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach($someData as $oneData)
        {
        ?>
            <tr >
                <td><input name="checkbox[]" type="checkbox" id="checkbox[]"
                           value="<?php echo $oneData->id; ?>"></td>
                <td ><?php echo $serial; ?></td >
                <td ><?php echo $oneData->id; ?></td >
                <td ><?php echo $oneData->user_name; ?></td >
                <td ><?php echo $oneData->user_city; ?></td >
                <td><a href="show.php?id=<?php echo $oneData->id; ?>"><button type="button" class="btn btn-primary btn-md" name="edit">View</button></a>
                <a href="edit.php?id=<?php echo $oneData->id; ?>"><button type="button" class="btn btn-success btn-md" name="edit">Edit</button></a>
                <a href="trash.php?id=<?php echo $oneData->id; ?>"><button type="button" class="btn btn-warning btn-md" name="edit">Trash</button></a>
                <a href="delete.php?id=<?php echo $oneData->id; ?>"><button type="button" class="btn btn-danger btn-md" name="delete">Delete</button></a></td>
            </tr >
        <?php
        $serial++;
        }
        ?>
        </tbody>
    </table>
    </div>
    </form>
    <!--  ######################## pagination code block#2 of 2 start ###################################### -->
    <div align="right" class="container" style="width: 990px">
        <ul class="pagination">

            <?php
            $prev = $page - 1;
            $next = $page + 1;
            $lastpage = ceil($recordCount/$itemsPerPage);      //lastpage is = total pages / items per page, rounded up.

            if($page>1)
                echo "<li><a href='index.php?Page=$prev'>" . "Previous" . '</a></li>';


            else
                echo '<li class="disabled"><a href="">'. "Previous" . '</a></li>';

            for($i=1;$i<=$pages;$i++)
            {

                if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

            }

            if($page<$lastpage)
                echo "<li><a href='?Page=$next'>" . "Next" . '</a></li>';
            else
                echo '<li class="disabled"><a href="">'. "Next" . '</a></li>';

            ?>

            <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
                <?php
                if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

                if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
                else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

                if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

                if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

                if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

                if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
                else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
                ?>
            </select>
        </ul>
    </div>
    <!--  ######################## pagination code block#2 of 2 end ###################################### -->
</div>

</body>
</html>

<!-- jQuery -->
<script src="../../../resource/left_nevigation_asset/js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="../../../resource/left_nevigation_asset/js/bootstrap.min.js"></script>
<!-- Morris Charts JavaScript -->
<script src="../../../resource/left_nevigation_asset/js/plugins/morris/raphael.min.js"></script>
<script src="../../../resource/left_nevigation_asset/js/plugins/morris/morris.min.js"></script>
<script src="../../../resource/left_nevigation_asset/js/plugins/morris/morris-data.js"></script>
<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>
<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->

<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->

<script language="javascript">
    function validate()
    {
        var chks = document.getElementsByName('checkbox[]');
        var hasChecked = false;
        for (var i = 0; i < chks.length; i++)
        {
            if (chks[i].checked)
            {
                hasChecked = true;
                break;
            }
        }
        if (hasChecked == false)
        {
            alert("Please select at least one.");
            return false;
        }
        return true;
    }
</script>

