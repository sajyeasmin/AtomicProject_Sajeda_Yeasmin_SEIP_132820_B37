<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\city\City;

if(!isset( $_SESSION)) session_start();
echo Message::message();

$objCity=new City();
$objCity->setData($_GET);
$singleItem= $objCity->view("obj");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Bootstrap Core CSS -->
    <link href="../../../resource/left_nevigation_asset/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../resource/left_nevigation_asset/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../../../resource/left_nevigation_asset/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../resource/left_nevigation_asset/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body background="../../../resource/background.jpg">
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Navigation -->
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">SB Admin</a>
            </div>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="../BookTitle/index.php"><i class="fa fa-fw fa-book"></i>Book Title</a>
                    </li>
                    <li>
                        <a href="../Birthday/index.php"><i class="fa fa-fw fa-birthday-cake"></i>Birthday</a>
                    </li>
                    <li class="active">
                        <a href="index.php"><i class="fa fa-fw fa-building"></i>City</a>
                    </li>
                    <li>
                        <a href="../Email/index.php"><i class="fa fa-fw fa-envelope"></i>Email</a>
                    </li>
                    <li>
                        <a href="../Gender/index.php"><i class="fa fa-fw fa-female"></i>Gender</a>
                    </li>
                    <li>
                        <a href="../Hobbies/index.php"><i class="fa fa-fw fa-gamepad"></i>Hobbies</a>
                    </li>
                    <li>
                        <a href="../Profile_Picture/index.php"><i class="fa fa-fw fa-user"></i>Profile Picture</a>
                    </li>
                    <li>
                        <a href="../Summary_Of_Organization/index.php"><i class="fa fa-fw fa-group"></i>Summary of Organization</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
</div>
<br>
<div class="container" style="width:1000px; margin-left: 250px;">
    <br>
    <div style="width: 1000px; height: 60px;margin-left: 100px;margin-top: 50px">
        <a href="index.php" class="btn btn-primary" role="button">Back To List</a>
    </div>
    <div style="border: 2px solid black;background-color:purple;color: white;margin-left: 30px;
    width: 500px;height: 50px">
        <p style="padding-left: 10px;text-align: center;font-size: 20px;margin-top: 5px"><b>City</b></p>
    </div>
    <form class="form-horizontal" action="update.php" method="post"
          style="border: 1px solid black;alignment: center;background-color:#F6EEF9 ;
     margin-left: 30px; width: 500px">

        <div class="col-xs-10" style="margin-left: 10px; margin-top: 10px">
            <input type="hidden" name="id" value="<?php echo $singleItem->id;?>">
            <div class="form-group">
                <label class="control-label col-sm-12" for="userName" style="text-align: left">User Name: </label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="user_name" placeholder="Enter User Name" value="<?php echo $singleItem->user_name;?>">
                </div>
            </div>

            <div class="form-group">
                <!--<label class="control-label col-sm-2" for="city">City: </label>-->
                <label class="control-label col-sm-12" for="city" style="text-align: left">Select country:</label>
                <div class="col-sm-12">

                    <select class="form-control" name ="state" id ="state">
                        <option><?php echo $singleItem->user_city;?></option>
                        <option>Dhaka</option>
                        <option>Chittagong</option>
                        <option>Rajshahi</option>
                        <option>Sylhet</option>
                        <option>Khulna</option>
                        <option>Barishal</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group" style="margin-left: 12px">
            <div class="col-sm-12" style="">
                <button type="submit" class="btn btn-primary btn-md" style="background-color: purple">Save</button>
            </div>
        </div>

    </form>
    <br>
</div>

</body>
</html>
