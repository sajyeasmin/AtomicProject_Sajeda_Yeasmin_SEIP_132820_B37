<?php
require_once("../../../vendor/autoload.php");
use App\city\City;

$objCity=new City();


if(isset($_POST['delete'])) {

    for ($i = 0; $i < count($_POST['checkbox']); $i++) {
        $del_id = $_POST['checkbox'][$i];
        $objCity->deleteSelected($del_id);
    }
}
else if(isset($_POST['trash'])){
    for ($i = 0; $i < count($_POST['checkbox']); $i++) {
        $trash_id = $_POST['checkbox'][$i];
        $objCity->trashSelected($trash_id);
    }
}
else if(isset($_POST['recovery'])){
    for ($i = 0; $i < count($_POST['checkbox']); $i++) {
        $trash_id = $_POST['checkbox'][$i];
        $objCity->restoreSelected($trash_id);
    }
}