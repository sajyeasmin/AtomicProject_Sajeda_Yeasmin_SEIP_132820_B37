<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Bootstrap Example</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script>
		$(document).ready(function(){
			$("#msg").delay(2500).fadeOut("slow");
		});
	</script>
</head>
<body>
<br>
<div class="container" style="width:600px; background-color: white">
	<br>
	<div style="border: 2px solid black;background-color:purple;color: white;margin-left: 30px;
    width: 500px;height: 50px">
		<p style="padding-left: 10px;text-align: center;font-size: 20px;margin-top: 5px"><b>City</b></p>
	</div>

	<form class="form-horizontal" action="store.php" method="post"
		  style="border: 1px solid black;alignment: center;background-color:#F6EEF9 ;
     margin-left: 30px; width: 500px">

		<div class="col-xs-10" style="margin-left: 10px; margin-top: 10px">

			<div class="form-group">
				<label class="control-label col-sm-12" for="bookName" style="text-align: left">User Name: </label>
				<div class="col-sm-12">
					<input type="text" class="form-control" name="user_name" placeholder="Enter User Name">
				</div>
			</div>

			<div class="form-group">
				<!--<label class="control-label col-sm-2" for="city">City: </label>-->
				<label class="control-label col-sm-12" for="city" style="text-align: left">Select country:</label>
				<div class="col-sm-12">

					<select class="form-control" name ="state" id ="state">
						<option>Dhaka</option>
						<option>Chittagong</option>
						<option>Rajshahi</option>
						<option>Sylhet</option>
						<option>Khulna</option>
						<option>Barishal</option>
					</select>
				</div>
			</div>
		</div>
		<div class="form-group" style="margin-left: 12px">
			<div class="col-sm-12" style="">
				<button type="submit" class="btn btn-primary btn-md" style="background-color: purple">Save</button>
			</div>
		</div>

	</form>
	<br>
</div>

</body>
</html>

