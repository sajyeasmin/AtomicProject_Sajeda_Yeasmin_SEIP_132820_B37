<?php

include_once('../../../vendor/autoload.php');
use App\email\Email;
use App\Message\Message;
use App\Utility\Utility;

$objEmail= new Email();

$id=$_POST['id'];

if($_SERVER['REQUEST_METHOD']=='POST')
{
    if(preg_match("/([A-Za-z0-9-_])/",$_POST['user_name'])) {
        $objEmail->setData($_POST);
        $objEmail->update();

    }
    else{
        Message::message("<div id='message'><h3 align='center'>Invalid Input !</h3></div>");
        header("location:edit.php?id=$id");
    }

}
else{
    Message::message("<div id='message'><h3 align='center'> Oops something went wrong !</h3></div>");
    Utility::redirect('index.php');
}