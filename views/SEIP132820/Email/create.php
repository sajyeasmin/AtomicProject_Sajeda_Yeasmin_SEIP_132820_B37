<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Add- Email - Formoid css form</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
			$("#msg").delay(2500).fadeOut("slow");
		});
	</script>
</head>

<body class="blurBg-false" style="background-color:#EBEBEB">

<!-- Start Formoid form-->
<link rel="stylesheet" href="../../../resource/form_booktitle_files/formoid1/formoid-solid-dark.css" type="text/css" />
<script type="text/javascript" src="../../../resource/form_booktitle_files/formoid1/jquery.min.js"></script>

<form id="formoid" class="formoid-solid-dark" action="store.php" method="post" style="background-color:#ffffff;font-size:14px;font-family:'Roboto',Arial,Helvetica,sans-serif;
		color:#34495E;max-width:780px;min-width:150px"><div class="title"><h2>Add- Email</h2></div>
	<div class="element-input" title="Please Enter Your Name."><label class="title"><span class="required">*</span></label><div class="item-cont"><input class="large" type="text" name="user_name" required="required" placeholder="User Name"/><span class="icon-place"></span></div></div>
	<div class="element-email"><label class="title"><span class="required">*</span></label><div class="item-cont"><input class="large" type="email" name="user_email" required="required" placeholder="Email"/><span class="icon-place"></span></div></div>
<br>
	<div class="submit">
	<input type="submit" name="action" value="Save"/>
		<input type="submit" name="action" value="Save and Add New" formaction="test.php"/>
		<input type="reset" name="action" value="Reset"/>
		<input type="submit" name="action" value="Back To List"/>
	</div>
	<br>
</form>
<p class="frmd"><a href="http://formoid.com/v29.php">css form</a> Formoid.com 2.9</p><script type="text/javascript" src="../../../resource/form_booktitle_files/formoid1/formoid-solid-dark.js"></script>
<!-- Stop Formoid form-->

</body>
</html>
