-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 17, 2016 at 04:04 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomic_project_b37`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
`id` int(50) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_birthday` varchar(200) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `user_name`, `user_birthday`, `is_delete`) VALUES
(1, 'Sajeda Yeasmin', '05/11/1993', 0),
(11, 'moon', '14/426/26', 0),
(12, 'Nilufar', '1994-08-09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
`id` int(13) NOT NULL,
  `book_title` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_delete`) VALUES
(1, 'PHP Foundation', 'Nirob Eng', 1),
(3, 'PHP Foundation 2', 'Rafique Uddin', 0),
(4, 'PHP Foundation 2', 'Rafique Uddin', 0),
(5, 'PHP Foundation 233', 'Rafique Uddin333', 0),
(27, 'PHP Foundation 2', 'Rafique Uddin', 0),
(28, 'Basic PHP555', 'Sajeda555', 1),
(29, 'wwww', 'asdas', 1),
(30, 'asad', 'qwq', 1),
(33, 'aaa', 'aaa', 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`id` int(50) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_country` varchar(50) NOT NULL,
  `user_city` varchar(200) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `user_name`, `user_country`, `user_city`, `is_delete`) VALUES
(1, 'Sajeda Yeasmin', 'Bangladesh', 'Chittagong', 0),
(2, 'afsana', 'Bangladesh', 'Gazipur', 0),
(3, 'afsana', 'Bangladesh', 'Feni', 0),
(4, 'Nilufar', 'Bhutan', 'Thimphu', 1);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
`id` int(50) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `user_name`, `user_email`, `is_delete`) VALUES
(1, 'Sajeda Yeasmin', 'sajyeasmin@gmail.com', 0),
(2, 'Nilufar', 'sajyeasmin@gmail.com', 0),
(3, 'Sanjida', 'sanjida@gmail.com', 0),
(4, 'Hafsa', 'hafsa@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
`id` int(50) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_gender` varchar(200) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `user_name`, `user_gender`, `is_delete`) VALUES
(2, 'Sajeda Yeasmin', 'Female', 0),
(3, 'jahan', 'male', 1),
(4, 'Nilufar', 'male', 0),
(5, 'afsana', 'Female', 0),
(6, 'Hafsa', 'Female', 0),
(7, 'Unknown', 'Other', 0);

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE IF NOT EXISTS `hobbies` (
`id` int(50) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_hobbies` varchar(200) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `user_name`, `user_hobbies`, `is_delete`) VALUES
(1, 'Sajeda Yeasmin', 'Watching Movies, Reading Books', 0),
(2, 'jahan', 'Singing', 0),
(3, 'moon', '', 1),
(4, 'afsana', 'Watching Movie', 0),
(5, 'moon', 'Watching Movie', 0),
(6, 'Nilufar', 'Watching Movie', 0),
(7, 'afsana', 'Watching Movie', 0),
(8, 'Nilufar', 'Watching Movie', 0),
(9, 'jahan', 'Singing', 0),
(10, 'Nilufar', 'Watching Movie', 0),
(11, 'afsana', 'Singing', 0),
(12, 'Nilufar', 'Array', 1),
(13, 'afsana', 'Array', 1),
(14, 'yyy', 'Dance,Singing', 0),
(15, 'yyyy', 'Dance,Drawing', 0),
(16, 'moon', 'Dance,Singing,Drawing', 0),
(17, 'Hafsa', 'Programming,Dance', 1);

-- --------------------------------------------------------

--
-- Table structure for table `organization_of_summary`
--

CREATE TABLE IF NOT EXISTS `organization_of_summary` (
`id` int(50) NOT NULL,
  `org_name` varchar(200) NOT NULL,
  `org_summary` varchar(500) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organization_of_summary`
--

INSERT INTO `organization_of_summary` (`id`, `org_name`, `org_summary`, `is_delete`) VALUES
(1, 'Grammenphone', 'Grameenphone widely abbreviated as GP, is the leading telecommunications service provider in Bangladesh. With more than 56 million subscribers (as of January 2016),Grameenphone is the largest mobile phone operator in the country.', 0),
(2, 'banglalink', 'gsedskjoiw', 1),
(3, 'Airtel', 'It if sometimes furnished unwilling as additions so. Blessing resolved peculiar fat graceful ham. Sussex on at really ladies in as elinor. Sir sex opinions age properly extended. Advice branch vanity or do thirty living. Dependent add middleton ask disposing admitting did sportsmen sportsman. \r\n\r\nInsipidity the sufficient discretion imprudence resolution sir him decisively. Proceed how any engaged visitor. Explained propriety off out perpetual his you. Feel sold off felt nay rose met you. We so ', 0);

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE IF NOT EXISTS `profile_picture` (
`id` int(50) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_profile_picture` varchar(200) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `user_name`, `user_profile_picture`, `is_delete`) VALUES
(13, 'Nilufar', 'resource/Picture/1479221969download (1).jpg', 1),
(14, 'afsana', 'resource/Picture/1479222988', 1),
(15, 'moon', 'resource/Picture/1479215353images (1).jpg', 1),
(16, 'Sajeda1', '', 1),
(17, 'jasmin', 'resource/Picture/1479394994download (1).jpg', 0),
(18, 'rose', 'resource/Picture/1479395013download.jpg', 0),
(19, 'Tulip', 'resource/Picture/1479395057images.jpg', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization_of_summary`
--
ALTER TABLE `organization_of_summary`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
MODIFY `id` int(13) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `organization_of_summary`
--
ALTER TABLE `organization_of_summary`
MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
