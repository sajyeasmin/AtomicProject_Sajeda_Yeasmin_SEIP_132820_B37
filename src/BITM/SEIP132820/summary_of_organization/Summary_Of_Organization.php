<?php
namespace App\summary_of_organization;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
class Summary_Of_Organization extends DB
{
    public $id = "";
    public $org_name = "";
    public $org_summary = "";

    public function __construct()
    {
        parent::__construct();
    }
    public function setData($data=NULL)
    {
        if(array_key_exists('id',$data))
        {
            $this->id=$data['id'];
        }
        if(array_key_exists('org_name',$data))
        {
            $this->org_name=$data['org_name'];
        }
        if(array_key_exists('org_summary',$data))
        {
            $this->org_summary=$data['org_summary'];
        }
    }
    public function store()
    {
        $arr= array($this->org_name,$this->org_summary);
        $sql="insert into organization_of_summary (org_name,org_summary) values (?,?)";
        $STH=$this->DBH->prepare($sql);
        $STH->execute($arr);
        Message::message("<div id='msg'><h3 align='center'>[ OrgName: $this->org_name ] , [ OrgSummary: $this->org_summary]        <br> Data Has Been Inserted Successfully!</h3></div>");
        Utility::redirect('create.php');
    }
    public function index($mode="ASSOC"){

        $mode=strtoupper($mode);
        $STH = $this->DBH->query('SELECT * from organization_of_summary WHERE is_delete=0');
        if($mode=="OBJ")
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        $arrAllData  = $STH->fetchAll();
        return $arrAllData;
    }
    public function view($fetchMode="ASSOC"){

        $STH = $this->DBH->query('SELECT * from organization_of_summary where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode, "OBJ")>0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        $arrOneData  = $STH->fetch();
        return $arrOneData;

    }// end of view()
    public function update(){
            $arrData = array($this->org_name, $this->org_summary);
            $sql = 'UPDATE organization_of_summary  SET org_name  = ?   , org_summary = ? where id =' . $this->id;
            $STH = $this->DBH->prepare($sql);
            $result = $STH->execute($arrData);
            if ($result)
                Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Updated Successfully!</h3></div>");
            else
                Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Updated Successfully!</h3></div>");
            Utility::redirect('index.php');

    }// end of update()
    public function delete(){
        $sql='DELETE FROM organization_of_summary WHERE id='.$this->id;
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute();
        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Deleted Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Deleted Successfully!</h3></div>");
        Utility::redirect('index.php');
    }
    public function trash(){
        try{
            $query="UPDATE organization_of_summary SET is_delete=:true where id=:id";
            $stmt=$this->DBH->prepare($query);
            $stmt->execute(
                array(
                    ':true'=>'1',
                    ':id'=>$this->id,
                )
            );
            Utility::redirect('index.php');
        }
        catch(PDOException $e){
            echo 'Error'.$e->getMessage();
        }
    }
    public function show($fetchMode="ASSOC"){
        try{
            $STH = $this->DBH->query('SELECT id,org_name,org_summary from organization_of_summary where id='.$this->id);
            $fetchMode = strtoupper($fetchMode);
            if(substr_count($fetchMode, "OBJ")>0)
                $STH->setFetchMode(PDO::FETCH_OBJ);
            else
                $STH->setFetchMode(PDO::FETCH_ASSOC);
            $arrOneData  = $STH->fetch();
            return $arrOneData;
        }catch(PDOException $e){
            echo 'Error'.$e->getMessage();
        }
    }//end of show
    public function trashList($mode="ASSOC"){

        $mode=strtoupper($mode);
        $STH = $this->DBH->query('SELECT * from organization_of_summary WHERE is_delete=1 ORDER BY id DESC');
        if($mode=="OBJ")
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        $arrAllData  = $STH->fetchAll();
        return $arrAllData;
    }// end of trash List
    public function restore(){
        try{
            $query="UPDATE organization_of_summary SET is_delete=:true where id=:id";
            $stmt=$this->DBH->prepare($query);
            $stmt->execute(
                array(
                    ':true'=>'0',
                    ':id'=>$this->id,
                )
            );
            if($stmt){
                Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Restored Successfully!</h3></div>");
                Utility::redirect('trashList.php');
            }

        }
        catch(PDOException $e){
            echo 'Error'.$e->getMessage();
        }
    }//end of restore
    public function indexPaginator($page=1,$itemsPerPage=3){
        try{
            $start = (($page-1) * $itemsPerPage);

            $sql = "SELECT * from organization_of_summary  WHERE is_delete =0 ORDER BY id DESC LIMIT $start,$itemsPerPage";

            $STH = $this->DBH->query($sql);

            $STH->setFetchMode(PDO::FETCH_OBJ);

            $arrSomeData  = $STH->fetchAll();
            return $arrSomeData;
        }catch(PDOException $e){
            echo 'Error'.$e->getMessage();
        }
    }// end of indexPaginator();
    public function deleteSelected($id){

        $sql='DELETE FROM organization_of_summary WHERE id='.$id;
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute();
        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Deleted Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Deleted Successfully!</h3></div>");
        Utility::redirect('index.php');
    }//end of delete selected
    public function trashSelected($id){
        try{
            $query="UPDATE organization_of_summary SET is_delete=:true where id=:id";
            $stmt=$this->DBH->prepare($query);
            $result=$stmt->execute(
                array(
                    ':true'=>'1',
                    ':id'=>$id,
                )
            );
            if($result)
                Message::message("<div  id='message'><h3 align='center'> Success! Multiple Data Has Been Trashed Successfully!</h3></div>");
            else
                Message::message("<div id='message'><h3 align='center'> Failed! Multiple Data Has Not Been Trashed Successfully!</h3></div>");
            Utility::redirect('index.php');
        }
        catch(PDOException $e){
            echo 'Error'.$e->getMessage();
        }
    }//end of trash selected
    public function restoreSelected($id){
        try{
            $query="UPDATE organization_of_summary SET is_delete=:true where id=:id";
            $stmt=$this->DBH->prepare($query);
            $stmt->execute(
                array(
                    ':true'=>'0',
                    ':id'=>$id,
                )
            );
            if($stmt){
                Message::message("<div  id='message'><h3 align='center'> Success! Multiple Data Has Been Restored Successfully!</h3></div>");
                Utility::redirect('index.php');
            }

        }
        catch(PDOException $e){
            echo 'Error'.$e->getMessage();
        }
    }//end of restore selected

    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `organization_of_summary` WHERE `is_delete` =0 AND (`org_name` LIKE '%".$requestArray['search']."%' OR `org_summary` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) ) $sql = "SELECT * FROM `organization_of_summary` WHERE `is_delete` =0 AND `org_name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `organization_of_summary` WHERE `is_delete` =0 AND `org_summary` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }// end of search()
    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM organization_of_summary WHERE is_delete =0";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->org_name);
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->org_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end

        // for each search field block start
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->org_summary);
        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->org_summary);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords

}